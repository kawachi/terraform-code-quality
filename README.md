# terraform-code-quality

Run GitLab's code quality against Terraform.

This script outputs a JSON with the specification described at https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool.

Currently, only tfsec is supported.

## How to use

Add following lines to your `.gitlab-ci.yml`:

```yaml
include:
  - remote: 'https://gitlab.com/kawachi/terraform-code-quality/-/raw/v0.0.2/Code-Quality.gitlab-ci.yml'
```

If you have changed the `stages`, specify the stage at which the `code_quality` job is run.

```yaml
code_quality:
  stage: stage_to_run
```

If you want to exclude certain rules of tfsec, you can set the `TFSEC_EXCLUDE` environment variable as a comma-separated list. This will be passed to the `--exclude` option of tfsec.

## fingerprint

The JSON contains a fingerprint. GitLab uses the fingerprint to determine if the generated warning is new.
Currently, it includes the file name.
If the same warning message is generated from different files, it has the advantage that we can treat each one as a separate entity.

## developer note

Don't forget to change the image version of `Code-Quality.gitlab-ci.yml` before creating a tag.
