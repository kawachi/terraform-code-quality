# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM alpine:3.5

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates bash jq

ADD https://github.com/tfsec/tfsec/releases/download/v0.36.6/tfsec-linux-amd64 /usr/local/bin/tfsec
ADD terraform-code-quality /usr/local/bin/
RUN chmod +x /usr/local/bin/tfsec /usr/local/bin/terraform-code-quality
